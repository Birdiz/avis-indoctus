-include docker/.env

CONTAINER ?= php

dev:
	cp .env.local .env

install: hooks
	cd ./docker && ./install.sh

start: hooks
	cp .env.local .env
	cd ./docker && ./start.sh
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console d:d:d --force --if-exists
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console d:d:c
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console d:s:c
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console hautelook:fixtures:load -n --no-bundles -vvv

stop:
	cd ./docker && ./stop.sh

restart: stop start

hooks:
	echo "#!/bin/bash" > .git/hooks/pre-commit
	echo "make check" >> .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

shell:
	cd ./docker && docker-compose exec $(CONTAINER) bash

warmup-cache:
	bin/console cache:warmup

check:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) make fixer
	cd ./docker/ && docker-compose exec -T $(CONTAINER) make phpstan
	cd ./docker/ && docker-compose exec -T $(CONTAINER) make twig-lint

fixer:
	tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src --rules=@Symfony

phpstan:
	bin/console cache:warmup --env=dev
	vendor/bin/phpstan analyse src --memory-limit=4G

twig-lint:
	bin/console lint:twig ./templates

test:
	cp .env.test .env
	cd ./docker/ && docker-compose exec -T $(CONTAINER) ./vendor/bin/phpunit --testdox --coverage-html var/tests/coverage
	cp .env.local .env

update:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) composer update

encore:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) yarn encore dev
