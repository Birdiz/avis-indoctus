import $ from 'jquery';
import '../css/form.css';

$(document).ready(function() {
  let max_fields = 3;
  let wrapper = $(".avis-quote-sources");
  let add_button = $(".add-form-field");

  let x = 1;
  $(add_button).click(function(e) {
    e.preventDefault();

    if (x <= max_fields) {
      x++;
      $(wrapper).append('<div class="control has-icons-left">\n' +
          '                            <input type="text"\n' +
          '                                   name="quote[sources][]"\n' +
          '                                   class="input"\n' +
          '                            >\n' +
          '                            <span class="icon is-small is-left">\n' +
          '                                <i class="fas fa-paperclip"></i>\n' +
          '                            </span>\n' +
          '                        <a href="#" class="delete"></a></div>');
    }
  });

  $(wrapper).on("click", ".delete", function(e) {
    e.preventDefault();

    $(this).parent('div').remove();
    x--;
  });

  $(function() {
    $("#media").change(function() {
      const filename = $(this).val().split('\\').pop();
      $(this).parent().find(".file-name").html(filename);
    });
  });
});
