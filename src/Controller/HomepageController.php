<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\FolderRepository;
use App\Service\QuoteManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class HomepageController extends AbstractController
{
    private QuoteManager $quoteManager;
    private FolderRepository $folderRepository;
    private SerializerInterface $serializer;

    public function __construct(
        QuoteManager $quoteManager,
        FolderRepository $folderRepository,
        SerializerInterface $serializer
    ) {
        $this->quoteManager = $quoteManager;
        $this->folderRepository = $folderRepository;
        $this->serializer = $serializer;
    }

    #[Route('/{_locale}', name: 'homepage', defaults: ['_locale' => 'fr'])]
    public function index(): Response
    {
        $folders = (new JsonEncoder())->decode(
            $this->serializer->serialize(
                $this->folderRepository->findAll(),
                JsonEncoder::FORMAT,
                ['groups' => 'folders']
            ),
            JsonEncoder::FORMAT
        );

        return $this->render('homepage/index.html.twig', [
            'quote' => $this->quoteManager->getRandom(),
            'folders' => $folders,
        ]);
    }
}
