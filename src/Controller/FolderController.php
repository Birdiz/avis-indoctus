<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Folder;
use App\Model\BlogController;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class FolderController extends BlogController
{
    private ArticleRepository $articleRepository;

    public function __construct(TranslatorInterface $translator, ArticleRepository $articleRepository)
    {
        parent::__construct($translator);

        $this->articleRepository = $articleRepository;
    }

    /**
     * @Route("/{_locale}/folders/{id}", name="folder", methods={"GET"})
     */
    public function index(Folder $folder): Response
    {
        return $this->render('folder/index.html.twig', [
            'folder' => $folder,
            'articles' => $this->articleRepository->getPublishedByFolder($folder),
            'breadcrumb' => [
                $this->translator->trans('folder.folders'),
                [
                    'path' => 'folder',
                    'id' => $folder->getId(),
                    'label' => $folder->getName(),
                    'isActive' => true,
                ],
            ],
        ]);
    }
}
