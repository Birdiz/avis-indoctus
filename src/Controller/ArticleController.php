<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Folder;
use App\Entity\Tag;
use App\Model\BlogController;
use App\Repository\ArticleRepository;
use App\Repository\TagRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArticleController extends BlogController
{
    private ArticleRepository $articleRepository;
    private TagRepository $tagRepository;

    public function __construct(
        TranslatorInterface $translator,
        ArticleRepository $articleRepository,
        TagRepository $tagRepository
    ) {
        parent::__construct($translator);

        $this->articleRepository = $articleRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @Route("/{_locale}/articles/{id}", name="article", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function index(Article $article): Response
    {
        $folder = $article->getFolder();
        if (false === $folder instanceof Folder) {
            throw new BadRequestHttpException('An article must have a folder.');
        }

        return $this->render('article/index.html.twig', [
            'article' => $article,
            'breadcrumb' => [
                $this->translator->trans('folder.folders'),
                [
                    'path' => 'folder',
                    'id' => $folder->getId(),
                    'label' => $folder->getName(),
                    'isActive' => false,
                ],
                $this->translator->trans('article.articles'),
                [
                    'path' => 'article',
                    'id' => $article->getId(),
                    'label' => $article->getTitle(),
                    'isActive' => true,
                ],
            ],
        ]);
    }

    /**
     * @Route("/{_locale}/articles/{slug}", name="article_search", methods={"GET"})
     */
    public function search(string $slug): Response
    {
        $tag = $this->tagRepository->findOneBy(['slug' => $slug]);

        if (false === $tag instanceof Tag) {
            throw new NotFoundHttpException();
        }

        return $this->render('article/search.html.twig', [
            'tag' => $tag,
            'articles' => $this->articleRepository->getAllByTag($tag),
            'breadcrumb' => [
                $this->translator->trans('article.articles'),
                $tag->getSlug(),
            ],
        ]);
    }
}
