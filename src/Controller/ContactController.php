<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Service\MailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    private MailManager $mailManager;

    public function __construct(MailManager $mailManager)
    {
        $this->mailManager = $mailManager;
    }

    /**
     * @Route("/{_locale}/contact", name="contact")
     */
    public function index(Request $request): Response
    {
        $isSent = false;
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            $isSent = $this->mailManager->sendMail(
                $contact['email'],
                null,
                'Nouveau message de '.$contact['name'],
                $contact['message']
            );
        }

        return $this->render(
            'contact/index.html.twig',
            [
                'form' => $form->createView(),
                'sent' => $isSent,
            ]
        );
    }
}
