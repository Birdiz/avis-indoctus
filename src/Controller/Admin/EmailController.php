<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Email;
use App\Form\EmailType;
use App\Service\EmailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/email", name="admin_")
 */
class EmailController extends AbstractController
{
    private EmailManager $emailManager;

    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * @Route("/", name="email_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/email/index.html.twig', [
            'emails' => $this->emailManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="email_show", methods={"GET"})
     */
    public function show(Email $email): Response
    {
        return $this->render('admin/email/show.html.twig', [
            'email' => $email,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="email_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Email $email): Response
    {
        $form = $this->createForm(EmailType::class, $email);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->emailManager->update($email);

            return $this->redirectToRoute('admin_email_show', ['id' => $email->getId()]);
        }

        return $this->render('admin/email/edit.html.twig', [
            'email' => $email,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="email_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Email $email): Response
    {
        if ($this->isCsrfTokenValid('delete'.$email->getId(), $request->request->get('_token'))) {
            $this->emailManager->delete($email);
        }

        return $this->redirectToRoute('admin_email_index');
    }
}
