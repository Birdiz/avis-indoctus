<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Quote;
use App\Form\QuoteType;
use App\Model\AdminController;
use App\Service\QuoteManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/quotes", name="admin_")
 */
final class QuoteController extends AdminController
{
    private QuoteManager $quoteManager;

    public function __construct(UserManager $userManager, QuoteManager $quoteManager)
    {
        parent::__construct($userManager);

        $this->quoteManager = $quoteManager;
    }

    /**
     * @Route("/", name="quote_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/quote/index.html.twig', [
            'quotes' => $this->quoteManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="quote_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $quote = new Quote();
        $form = $this->createForm(QuoteType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->quoteManager->create($quote);

            return $this->redirectToRoute('admin_quote_index', ['id' => $quote->getId()]);
        }

        return $this->render('admin/quote/new.html.twig', [
            'quote' => $quote,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="quote_show", methods={"GET"})
     */
    public function show(Quote $quote): Response
    {
        return $this->render('admin/quote/show.html.twig', [
            'quote' => $quote,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="quote_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Quote $quote): Response
    {
        $form = $this->createForm(QuoteType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->quoteManager->update($quote);

            return $this->redirectToRoute('admin_quote_index', ['id' => $quote->getId()]);
        }

        return $this->render('admin/quote/edit.html.twig', [
            'quote' => $quote,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="quote_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Quote $quote): Response
    {
        if ($this->isCsrfTokenValid('delete'.$quote->getId(), $request->request->get('_token'))) {
            $this->quoteManager->delete($quote);
        }

        return $this->redirectToRoute('admin_quote_index');
    }
}
