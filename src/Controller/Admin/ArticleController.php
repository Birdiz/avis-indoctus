<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Model\AdminController;
use App\Repository\FolderRepository;
use App\Repository\TagRepository;
use App\Service\ArticleManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/articles", name="admin_")
 */
final class ArticleController extends AdminController
{
    private ArticleManager $articleManager;
    private TagRepository $tagRepository;
    private FolderRepository $folderRepository;

    public function __construct(
        UserManager $userManager,
        ArticleManager $articleManager,
        TagRepository $tagRepository,
        FolderRepository $folderRepository
    ) {
        parent::__construct($userManager);

        $this->articleManager = $articleManager;
        $this->tagRepository = $tagRepository;
        $this->folderRepository = $folderRepository;
    }

    /**
     * @Route("/", name="article_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/article/index.html.twig', [
            'articles' => $this->articleManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->articleManager->create($article);

            return $this->redirectToRoute('admin_article_show', ['id' => $article->getId()]);
        }

        return $this->render('admin/article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
            'tags' => $this->tagRepository->findAll(),
            'folders' => $this->folderRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_show", methods={"GET"})
     */
    public function show(Article $article): Response
    {
        return $this->render('admin/article/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->articleManager->update($article);

            return $this->redirectToRoute('admin_article_show', ['id' => $article->getId()]);
        }

        return $this->render('admin/article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
            'tags' => $this->tagRepository->findAll(),
            'folders' => $this->folderRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $this->articleManager->delete($article);
        }

        return $this->redirectToRoute('admin_article_index');
    }
}
