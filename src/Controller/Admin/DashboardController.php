<?php

namespace App\Controller\Admin;

use App\Model\AdminController;
use App\Service\DashboardManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
final class DashboardController extends AdminController
{
    private DashboardManager $dashboardManager;

    public function __construct(UserManager $userManager, DashboardManager $dashboardManager)
    {
        parent::__construct($userManager);

        $this->dashboardManager = $dashboardManager;
    }

    /**
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard/index.html.twig', [
            'totals' => $this->dashboardManager->getKPIs(),
        ]);
    }
}
