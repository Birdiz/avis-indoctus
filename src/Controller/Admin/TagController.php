<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Tag;
use App\Form\TagType;
use App\Model\AdminController;
use App\Service\TagManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/tags", name="admin_")
 */
final class TagController extends AdminController
{
    private TagManager $tagManager;

    public function __construct(UserManager $userManager, TagManager $tagManager)
    {
        parent::__construct($userManager);

        $this->tagManager = $tagManager;
    }

    /**
     * @Route("/", name="tag_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/tag/index.html.twig', [
            'tags' => $this->tagManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tag_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->tagManager->create($tag);

            return $this->redirectToRoute('admin_tag_index', ['id' => $tag->getId()]);
        }

        return $this->render('admin/tag/new.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
            'parents' => $this->tagManager->getRepository()->findAll(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="tag_show", methods={"GET"})
     */
    public function show(Tag $tag): Response
    {
        return $this->render('admin/tag/show.html.twig', [
            'tag' => $tag,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tag_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->tagManager->update($tag);

            return $this->redirectToRoute('admin_tag_index', ['id' => $tag->getId()]);
        }

        return $this->render('admin/tag/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
            'parents' => $this->tagManager->getRepository()->findAll(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="tag_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tag $tag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tag->getId(), $request->request->get('_token'))) {
            $this->tagManager->delete($tag);
        }

        return $this->redirectToRoute('admin_tag_index');
    }
}
