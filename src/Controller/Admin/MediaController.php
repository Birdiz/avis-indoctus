<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Media;
use App\Form\MediaType;
use App\Model\AdminController;
use App\Service\MediaManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/media", name="admin_")
 */
final class MediaController extends AdminController
{
    private MediaManager $mediaManager;

    public function __construct(UserManager $userManager, MediaManager $mediaManager)
    {
        parent::__construct($userManager);

        $this->mediaManager = $mediaManager;
    }

    /**
     * @Route("/", name="media_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/media/index.html.twig', [
            'media' => $this->mediaManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="media_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $medium = new Media();
        $form = $this->createForm(MediaType::class, $medium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->mediaManager->create($medium);

            return $this->redirectToRoute('admin_media_show', ['id' => $medium->getId()]);
        }

        return $this->render('admin/media/new.html.twig', [
            'medium' => $medium,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="media_show", methods={"GET"})
     */
    public function show(Media $medium): Response
    {
        return $this->render('admin/media/show.html.twig', [
            'medium' => $medium,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="media_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Media $medium): Response
    {
        $form = $this->createForm(MediaType::class, $medium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->mediaManager->update($medium);

            return $this->redirectToRoute('admin_media_show', ['id' => $medium->getId()]);
        }

        return $this->render('admin/media/edit.html.twig', [
            'medium' => $medium,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="media_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Media $medium): Response
    {
        if ($this->isCsrfTokenValid('delete'.$medium->getId(), $request->request->get('_token'))) {
            $this->mediaManager->delete($medium);
        }

        return $this->redirectToRoute('admin_media_index');
    }
}
