<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Folder;
use App\Form\FolderType;
use App\Model\AdminController;
use App\Service\FolderManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/admin/folders", name="admin_")
 */
final class FolderController extends AdminController
{
    private FolderManager $folderManager;
    private SerializerInterface $serializer;

    public function __construct(UserManager $userManager, FolderManager $folderManager, SerializerInterface $serializer)
    {
        parent::__construct($userManager);

        $this->folderManager = $folderManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="folder_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('admin/folder/index.html.twig', [
            'folders' => $this->folderManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="folder_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $folder = new Folder();
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->folderManager->create($folder);

            return $this->redirectToRoute('admin_folder_show', ['id' => $folder->getId()]);
        }

        return $this->render('admin/folder/new.html.twig', [
            'folder' => $folder,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="folder_show", methods={"GET"})
     */
    public function show(Folder $folder): Response
    {
        $folder = (new JsonEncoder())->decode(
            $this->serializer->serialize($folder, JsonEncoder::FORMAT, ['groups' => 'folders']),
            JsonEncoder::FORMAT
        );

        return $this->render('admin/folder/show.html.twig', [
            'folder' => $folder,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="folder_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Folder $folder): Response
    {
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->folderManager->update($folder);

            return $this->redirectToRoute('admin_folder_show', ['id' => $folder->getId()]);
        }

        return $this->render('admin/folder/edit.html.twig', [
            'folder' => $folder,
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
        ]);
    }

    /**
     * @Route("/{id}", name="folder_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Folder $folder): Response
    {
        if ($this->isCsrfTokenValid('delete'.$folder->getId(), $request->request->get('_token'))) {
            $this->folderManager->delete($folder);
        }

        return $this->redirectToRoute('admin_folder_index');
    }
}
