<?php

namespace App\Controller;

use App\Form\EmailType;
use App\Service\EmailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractController
{
    private EmailManager $emailManager;

    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * @Route("/{_locale}/newsletter", name="newsletter")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(EmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->emailManager->createAndAlert($form->getData());

            return $this->redirectToRoute('newsletter_congrats', ['_locale' => $request->getLocale()]);
        }

        return $this->render(
            'newsletter/index.html.twig',
            [
                'form' => $form->createView(),
                'errors' => $form->getErrors(true),
            ]
        );
    }

    /**
     * @Route("/{_locale}/newsletter/congrats", name="newsletter_congrats", methods={"GET"})
     */
    public function congrats(): Response
    {
        return $this->render('newsletter/newsletter_congrats.html.twig');
    }
}
