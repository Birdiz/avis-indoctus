<?php

namespace App\Model;

use App\Contract\CountableRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class CountableRepository extends ServiceEntityRepository implements CountableRepositoryInterface
{
}
