<?php

namespace App\Model;

use App\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AdminController extends AbstractController
{
    protected UserManager $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }
}
