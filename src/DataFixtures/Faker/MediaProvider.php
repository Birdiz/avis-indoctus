<?php

namespace App\DataFixtures\Faker;

use Faker\Provider\Base as BaseProvider;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaProvider extends BaseProvider
{
    public const IMG_PATH = __DIR__.'/images';

    /**
     * Create and move files for every needed Media for the fixtures.
     */
    public static function getMediaFile(string $name, string $path): UploadedFile
    {
        $fileSystem = new Filesystem();
        if (false === $fileSystem->exists(self::IMG_PATH.'/tmp/')) {
            $fileSystem->mkdir(self::IMG_PATH.'/tmp/');
        }

        copy(self::IMG_PATH.'/'.$path, self::IMG_PATH.'/tmp/'.$path);

        return new UploadedFile(self::IMG_PATH.'/tmp/'.$path, $name, null, null, true);
    }
}
