<?php

namespace App\DataFixtures\Processor;

use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateProcessor implements ProcessorInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function preProcess(string $id, $object): void
    {
        $this->validator->validate($object);
    }

    public function postProcess(string $id, $object): void
    {
    }
}
