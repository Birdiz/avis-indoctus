<?php

namespace App\DataFixtures\Processor;

use App\Entity\User;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserProcessor implements ProcessorInterface
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function preProcess(string $id, $object): void
    {
        if ($object instanceof User) {
            $object->setPassword(
                $this->passwordHasher->hashPassword($object, $object->getPassword())
            );
        }
    }

    public function postProcess(string $id, $object): void
    {
    }
}
