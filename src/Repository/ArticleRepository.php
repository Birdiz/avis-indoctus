<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Folder;
use App\Entity\Tag;
use App\Model\CountableRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends CountableRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function countAll(): int
    {
        return $this
            ->createQueryBuilder('a')
            ->select('count(a)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countAllFromFolder(Folder $folder, bool $isPublished = true): int
    {
        $q = $this
            ->createQueryBuilder('a')
            ->select('count(a)')
            ->where('a.folder = :folder')
            ->setParameter('folder', $folder);

        $status = $isPublished ? Article::PUBLISHED : Article::DRAFT;

        $q
            ->andWhere('a.status = :status')
            ->setParameter('status', $status);

        return $q
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return array<Article>
     */
    public function getPublishedByFolder(Folder $folder): array
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.folder = :folder AND a.status = :status')
            ->setParameters(
                [
                    'folder' => $folder,
                    'status' => Article::PUBLISHED,
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array<Article>
     */
    public function getAllByTag(Tag $tag): array
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->where('t.id = :id AND a.status = :status')
            ->setParameters(
                [
                    'id' => $tag->getId(),
                    'status' => Article::PUBLISHED,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
