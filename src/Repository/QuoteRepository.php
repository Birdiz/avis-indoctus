<?php

namespace App\Repository;

use App\Entity\Quote;
use App\Model\CountableRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Quote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quote[]    findAll()
 * @method Quote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuoteRepository extends CountableRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quote::class);
    }

    public function countAll(): int
    {
        return $this
            ->createQueryBuilder('q')
            ->select('count(q)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
