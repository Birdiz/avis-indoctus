<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager extends BaseManager
{
    private Security $security;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(
        EntityManagerInterface $objectManager,
        LogManager $logManager,
        Security $security,
        UserPasswordHasherInterface $passwordHasher
    ) {
        parent::__construct($objectManager, $logManager);

        $this->security = $security;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @required
     */
    public function setEntityName(string $entityName = User::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    public function getCurrentUser(): UserInterface
    {
        if (!($user = $this->security->getUser())) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }

    public function encodePassword(User $user): void
    {
        $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPassword()));
    }
}
