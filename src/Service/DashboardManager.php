<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ArticleRepository;
use App\Repository\EmailRepository;
use App\Repository\QuoteRepository;
use App\Repository\UserRepository;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardManager
{
    private TranslatorInterface $translator;
    private UserRepository $userRepository;
    private ArticleRepository $articleRepository;
    private QuoteRepository $quoteRepository;
    private EmailRepository $emailRepository;

    public function __construct(
        TranslatorInterface $translator,
        UserRepository $userRepository,
        ArticleRepository $articleRepository,
        QuoteRepository $quoteRepository,
        EmailRepository $emailRepository
    ) {
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->articleRepository = $articleRepository;
        $this->quoteRepository = $quoteRepository;
        $this->emailRepository = $emailRepository;
    }

    /**
     * @return array<string, int>
     */
    public function getKPIs(): array
    {
        return [
            $this->translator->trans('user.users') => $this->userRepository->countAll(),
            $this->translator->trans('article.articles') => $this->articleRepository->countAll(),
            $this->translator->trans('quote.quotes') => $this->quoteRepository->countAll(),
            $this->translator->trans('email.mail_list') => $this->emailRepository->countAll(),
        ];
    }
}
