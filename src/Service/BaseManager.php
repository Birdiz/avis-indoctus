<?php

declare(strict_types=1);

namespace App\Service;

use App\Contract\BaseManagerInterface;
use App\Contract\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use function get_class;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseManager implements BaseManagerInterface
{
    protected EntityManagerInterface $objectManager;
    protected string $entityName;
    protected LogManager $logManager;

    public function __construct(EntityManagerInterface $objectManager, LogManager $logManager)
    {
        $this->objectManager = $objectManager;
        $this->logManager = $logManager;
    }

    public function create(EntityInterface $entity): EntityInterface
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, 'Creation impossible, entity already exist.', true);

        return $this->save($entity);
    }

    public function update(EntityInterface $entity): EntityInterface
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, "Update impossible, entity doesn't exist.", false);

        return $this->save($entity);
    }

    public function delete(EntityInterface $entity): void
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, "Delete impossible, entity doesn't exist.", false);

        try {
            $this->objectManager->remove($entity);
            $this->objectManager->flush();
        } catch (Exception $e) {
            $this->logManager->logError(
                "{$e->getCode()} - {$e->getMessage()}",
                "Deleting $this->entityName with Id : {$entity->getId()}"
            );
            throw $e;
        }
    }

    public function get(?int $id): ?EntityInterface
    {
        if (null === $id) {
            $this->logManager->logError("Getting $this->entityName with Id null.");
            throw new BadRequestHttpException("Entity's Id can not be null");
        }

        $entity = $this->getRepository()->find($id);

        if (false === $entity instanceof EntityInterface) {
            $this->logManager->logError("Couldn't fetch $this->entityName with Id : $id");
            throw new NotFoundHttpException();
        }

        return $entity;
    }

    public function setEntityName(string $entityName): BaseManager
    {
        $this->entityName = $entityName;

        return $this;
    }

    public function getRepository(): EntityRepository|ObjectRepository
    {
        /* @phpstan-ignore-next-line */
        return $this->objectManager->getRepository($this->entityName);
    }

    private function save(EntityInterface $entity): EntityInterface
    {
        try {
            $this->objectManager->persist($entity);
            $this->objectManager->flush();
        } catch (Exception $e) {
            $this->logManager->logError(
                "{$e->getCode()} - {$e->getMessage()}",
                "Saving $this->entityName with Id : {$entity->getId()}"
            );
            throw $e;
        }

        return $entity;
    }

    private function preventBadEntityType(EntityInterface $entity): void
    {
        if (!$entity instanceof $this->entityName) {
            $class = get_class($entity);

            $this->logManager->logError("Bad entity $this->entityName for $class");
            throw new InvalidArgumentException("Bad entity type, $this->entityName given instead of $class");
        }
    }

    private function preventEntityState(EntityInterface $entity, string $msg, bool $exist): void
    {
        $class = get_class($entity);
        if ($exist xor !$entity->getId()) {
            $this->logManager->logError($msg, "$class with {$entity->getId()}");
            throw new RuntimeException($msg);
        }
    }
}
