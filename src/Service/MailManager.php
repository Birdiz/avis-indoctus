<?php

namespace App\Service;

use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class MailManager.
 */
class MailManager
{
    /** @var MailerInterface */
    private $mailer;

    /** @var LogManager */
    private $logManager;

    /** @var string */
    private $avisEmail;

    /**
     * MailManager constructor.
     */
    public function __construct(MailerInterface $mailer, LogManager $logManager, string $avisEmail)
    {
        $this->mailer = $mailer;
        $this->logManager = $logManager;
        $this->avisEmail = $avisEmail;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendMail(string $from, ?string $to, string $subject, string $message): bool
    {
        $email = (new Email())
            ->from($from)
            ->to(null !== $to ? $to : $this->avisEmail)
            ->subject($subject)
            ->text($message);

        try {
            $this->mailer->send($email);

            $this->logManager->logInfo("Mail sent to {$to} from {$from} with subject {$subject}");

            return true;
        } catch (Exception $e) {
            $this->logManager->logError(
                "{$e->getCode()} - {$e->getMessage()}",
                "Mail did not sent to {$to} from {$from} with subject {$subject}"
            );

            return false;
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendAutomaticMail(string $emailAddress, string $subject): bool
    {
        $email = (new TemplatedEmail())
            ->from($this->avisEmail)
            ->to($emailAddress)
            ->subject($subject)
            ->htmlTemplate('automatic/emails/newsletter_congrats.html.twig')
            ->context([
                'emailAddress' => $emailAddress,
            ])
        ;

        try {
            $this->mailer->send($email);

            $this->logManager->logInfo("Mail sent to {$this->avisEmail} from {$emailAddress} with subject {$subject}");

            return true;
        } catch (Exception $e) {
            $this->logManager->logError(
                "{$e->getCode()} - {$e->getMessage()}",
                "Mail did not sent to {$this->avisEmail} from {$emailAddress} with subject {$subject}"
            );

            return false;
        }
    }
}
