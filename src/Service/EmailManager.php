<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailManager extends BaseManager
{
    private MailManager $mailManager;
    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $objectManager,
        LogManager $logManager,
        MailManager $mailManager,
        TranslatorInterface $translator
    ) {
        parent::__construct($objectManager, $logManager);

        $this->mailManager = $mailManager;
        $this->translator = $translator;
    }

    /**
     * @required
     */
    public function setEntityName(string $entityName = Email::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    public function createAndAlert(Email $email): void
    {
        $this->create($email);

        $this->mailManager->sendAutomaticMail(
            $email->getEmail(),
            $this->translator->trans('app.newsletter_page.congrats')
        );
    }
}
