<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Article;

class ArticleManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Article::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }
}
