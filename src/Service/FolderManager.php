<?php

namespace App\Service;

use App\Entity\Folder;

/**
 * Class FolderManager.
 */
class FolderManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Folder::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }
}
