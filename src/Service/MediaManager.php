<?php

namespace App\Service;

use App\Entity\Media;

/**
 * Class MediaManager.
 */
class MediaManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Media::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }
}
