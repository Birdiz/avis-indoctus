<?php

namespace App\Service;

use App\Entity\Profile;

/**
 * Class ProfileManager.
 */
class ProfileManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Profile::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }
}
