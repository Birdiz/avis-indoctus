<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Quote;

class QuoteManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Quote::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    public function getRandom(): Quote
    {
        $quotes = $this->getRepository()->findAll();

        return $quotes[random_int(0, count($quotes) - 1)];
    }
}
