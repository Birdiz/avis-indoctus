<?php

namespace App\Service;

use App\Entity\Tag;

/**
 * Class TagManager.
 */
class TagManager extends BaseManager
{
    /**
     * @required
     */
    public function setEntityName(string $entityName = Tag::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }
}
