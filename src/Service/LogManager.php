<?php

namespace App\Service;

use DateTime;
use Exception;
use Psr\Log\LoggerInterface;

class LogManager
{
    private LoggerInterface $logger;
    private UserManager $userManager;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @required
     */
    public function setUserManager(UserManager $userManager): LogManager
    {
        $this->userManager = $userManager;

        return $this;
    }

    private function logStart(): void
    {
        $this->logger->info('Started At : '.(new DateTime())->format('Y-m-d H:i:s'));

        try {
            $this->logger->info('Thrown by : '.$this->userManager->getCurrentUser()->getUsername());
        } catch (Exception $e) {
            $this->logger->info('Thrown By : no granted user.');
            $this->logger->info($e->getCode().' - '.$e->getMessage());
        }
    }

    private function logEnd(): void
    {
        $this->logger->info('Ended At : '.(new DateTime())->format('Y-m-d H:i:s'));
    }

    public function logError(string $message, ?string $action = null): void
    {
        $this->logStart();

        $this->logger->error('AVIS ERROR');
        $this->logger->error($message);

        if ($action) {
            $this->logger->debug($action);
        }

        $this->logEnd();
    }

    public function logInfo(string $message): void
    {
        $this->logStart();

        $this->logger->info('AVIS INFO');
        $this->logger->info($message);

        $this->logEnd();
    }
}
