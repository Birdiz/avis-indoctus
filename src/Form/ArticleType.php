<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Folder;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('cover', MediaType::class)
            ->add('title')
            ->add('subtitle')
            ->add('caption')
            ->add('content')
            ->add(
                'tags',
                EntityType::class,
                [
                    'class' => Tag::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                ]
            )
            ->add(
                'folder',
                EntityType::class,
                [
                    'class' => Folder::class,
                    'choice_label' => 'name',
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'allow_extra_fields' => true,
        ]);
    }
}
