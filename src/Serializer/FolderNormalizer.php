<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\Folder;
use App\Repository\ArticleRepository;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FolderNormalizer implements ContextAwareNormalizerInterface
{
    private ObjectNormalizer $normalizer;
    private ArticleRepository $articleRepository;

    public function __construct(ObjectNormalizer $normalizer, ArticleRepository $articleRepository)
    {
        $this->normalizer = $normalizer;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param mixed $folder
     */
    public function normalize($folder, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($folder, $format, $context);

        $data['articlePublished'] = $this->articleRepository->countAllFromFolder($folder);
        $data['articleDraft'] = $this->articleRepository->countAllFromFolder($folder, false);

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Folder;
    }
}
