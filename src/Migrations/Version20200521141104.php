<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521141104 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE profile CHANGE avatar_id avatar_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media ADD created_by_id INT NOT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL, CHANGE media_name media_name VARCHAR(255) DEFAULT NULL, CHANGE media_original_name media_original_name VARCHAR(255) DEFAULT NULL, CHANGE media_mime_type media_mime_type VARCHAR(255) DEFAULT NULL, CHANGE media_size media_size INT DEFAULT NULL, CHANGE media_dimensions media_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6A2CA10CB03A8386 ON media (created_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10CB03A8386');
        $this->addSql('DROP INDEX UNIQ_6A2CA10CB03A8386 ON media');
        $this->addSql('ALTER TABLE media DROP created_by_id, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE media_name media_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE media_original_name media_original_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE media_mime_type media_mime_type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE media_size media_size INT DEFAULT NULL, CHANGE media_dimensions media_dimensions LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE profile CHANGE avatar_id avatar_id INT DEFAULT NULL');
    }
}
