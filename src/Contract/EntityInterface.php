<?php

namespace App\Contract;

interface EntityInterface
{
    public function getId(): ?int;
}
