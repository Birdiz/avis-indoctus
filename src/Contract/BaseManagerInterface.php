<?php

declare(strict_types=1);

namespace App\Contract;

interface BaseManagerInterface
{
    public function create(EntityInterface $entity): EntityInterface;

    public function update(EntityInterface $entity): EntityInterface;

    public function delete(EntityInterface $entity): void;

    public function get(?int $id): ?EntityInterface;
}
