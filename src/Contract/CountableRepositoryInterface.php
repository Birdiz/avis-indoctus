<?php

namespace App\Contract;

interface CountableRepositoryInterface
{
    public function countAll(): int;
}
