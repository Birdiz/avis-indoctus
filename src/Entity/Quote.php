<?php

declare(strict_types=1);

namespace App\Entity;

use App\Contract\EntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuoteRepository")
 */
class Quote implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private ?string $quote = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private ?string $author = null;

    /**
     * @var array<string>
     * @ORM\Column(type="json")
     * @Assert\NotNull()
     */
    private array $sources = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return array<string>|null
     */
    public function getSources(): ?array
    {
        return $this->sources;
    }

    /**
     * @param array<string> $sources
     */
    public function setSources(array $sources): self
    {
        $this->sources = $sources;

        return $this;
    }
}
