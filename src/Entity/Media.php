<?php

declare(strict_types=1);

namespace App\Entity;

use App\Contract\EntityInterface;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 * @Vich\Uploadable
 */
class Media implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Vich\UploadableField(
     *     mapping="media",
     *     fileNameProperty="media.name",
     *     size="media.size",
     *     mimeType="media.mimeType",
     *     originalName="media.originalName",
     *     dimensions="media.dimensions"
     * )
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" })
     */
    private ?File $mediaFile = null;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     */
    private EmbeddedFile $media;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $updatedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserInterface $createdBy = null;

    public function __construct()
    {
        $this->media = new EmbeddedFile();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMediaFile(): ?File
    {
        return $this->mediaFile;
    }

    public function setMediaFile(?File $mediaFile = null): self
    {
        $this->mediaFile = $mediaFile;

        if ($mediaFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }

    public function getMedia(): ?EmbeddedFile
    {
        return $this->media;
    }

    public function setMedia(EmbeddedFile $avatar): self
    {
        $this->media = $avatar;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?UserInterface
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserInterface $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
