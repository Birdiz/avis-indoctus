<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Article;
use App\Entity\Media;
use App\Service\UserManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\User\UserInterface;

class ArticleSubscriber implements EventSubscriber
{
    private UserManager $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (false === $entity instanceof Article) {
            return;
        }

        if ($entity->getCreatedBy()) {
            return;
        }

        $user = $this->userManager->getCurrentUser();
        $entity->setCreatedBy($user);
        $this->setCoverCreator($entity, $user);
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (false === $entity instanceof Article) {
            return;
        }

        $this->setCoverCreator($entity, $this->userManager->getCurrentUser());
    }

    private function setCoverCreator(Article $entity, UserInterface $user): void
    {
        if ($entity->getCover() instanceof Media && !$entity->getCover()->getCreatedBy()) {
            $entity->getCover()->setCreatedBy($user);
        }
    }
}
