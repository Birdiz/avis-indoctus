<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Media;
use App\Service\UserManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class MediaSubscriber implements EventSubscriber
{
    private UserManager $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (false === $entity instanceof Media) {
            return;
        }

        if ($entity->getCreatedBy()) {
            return;
        }

        $entity->setCreatedBy($this->userManager->getCurrentUser());
    }
}
