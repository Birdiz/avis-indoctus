<?php

namespace App\Tests;

use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AvisWebTestCase
 * @package App\Tests
 */
class AvisWebTestCase extends WebTestCase
{
    use RefreshDatabaseTrait;

    /**
     * @param array $options
     * @param array $server
     * @param bool $logAsAdmin
     * @return KernelBrowser
     */
    public static function createClient(array $options = [], array $server = [], bool $logAsAdmin = true): KernelBrowser
    {
        if ($logAsAdmin) {
            $server = array_merge(
                $server,
                [
                    'PHP_AUTH_USER' => 'admin@avis.com',
                    'PHP_AUTH_PW'   => 'admin',
                ]
            );
        }

        return parent::createClient($options, $server);
    }

}
