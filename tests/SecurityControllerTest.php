<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class SecurityControllerTest
 * @package App\Tests
 */
class SecurityControllerTest extends AvisWebTestCase
{
    public function testLogin(): void
    {
        $client = static::createClient([], [], false);

        $crawler = $client->request('GET', '/');

        static::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $loginLink = $crawler->selectLink('Connexion')->link();
        $crawler = $client->click($loginLink);

        static::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $loginForm = $crawler->selectButton('Connexion')->form();
        $loginForm['email'] = 'admin@avis.com';
        $loginForm['password'] = 'admin';

        $client->submit($loginForm);

        static::assertTrue($client->getResponse()->isRedirect('http://localhost/'));
    }

    public function testLogout(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        static::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $logoutLink = $crawler->selectLink('Déconnexion')->link();
        $client->click($logoutLink);

        static::assertTrue($client->getResponse()->isRedirect('http://localhost/'));
    }
}
