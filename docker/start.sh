#!/usr/bin/env bash

set -e

readonly DOCKER_PATH=$(dirname $(realpath $0))
cd ${DOCKER_PATH};

. ./lib/functions.sh
. ./.env

# Build all container in parallel to optimize your time
docker-compose build --parallel

# Start and remove useless containers
docker-compose up -d --remove-orphans

block_success "Yout project is started https://${HTTP_HOST}"
